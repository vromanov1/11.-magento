<?php

namespace Lachestry\Faq\Model;

use Lachestry\Faq\Api\Data\FaqInterface;
use Lachestry\Faq\Api\Data\FaqSearchResultInterfaceFactory;
use Lachestry\Faq\Api\FaqRepositoryInterface;
use Lachestry\Faq\Model\ResourceModel\Faq as FaqResource;
use Lachestry\Faq\Model\ResourceModel\Faq\CollectionFactory as FaqCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor\FilterProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

class FaqRepository implements FaqRepositoryInterface
{
    private $faqResource;
    private $faqFactory;
    private $faqCollectionFactory;
    private $faqSearchResultFactory;
    private $filterProcessor;

    public function __construct(
        FaqResource $faqResource,
        FaqFactory $faqFactory,
        FaqCollectionFactory $faqCollectionFactory,
        FaqSearchResultInterfaceFactory $faqSearchResultFactory,
        FilterProcessor $filterProcessor
    ) {
        $this->faqResource = $faqResource;
        $this->faqFactory = $faqFactory;
        $this->faqCollectionFactory = $faqCollectionFactory;
        $this->faqSearchResultFactory = $faqSearchResultFactory;
        $this->filterProcessor = $filterProcessor;
    }

    public function get(int $id): FaqInterface
    {
        $faq = $this->faqFactory->create();
        $this->faqResource->load($faq, $id);
        if (!$faq->getId()) {
            throw new NoSuchEntityException(__('Requested faq does not exist'));
        }

        return $faq;
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->faqCollectionFactory->create();
        $this->filterProcessor->process($searchCriteria, $collection);

        $searchResult = $this->faqSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

    public function save(FaqInterface $faq): FaqInterface
    {
        try {
            $this->faqResource->save($faq);
        } catch (\Exception $exception) {
            throw new StateException(__('Unable to save faq #%1', $faq->getId()));
        }

        return $faq;
    }

    public function delete(FaqInterface $faq): bool
    {
        try {
            $this->faqResource->delete($faq);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove faq #%1', $faq->getId()));
        }

        return true;
    }

    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }
}

