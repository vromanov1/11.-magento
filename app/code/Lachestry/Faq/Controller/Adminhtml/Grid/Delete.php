<?php

namespace Lachestry\Faq\Controller\Adminhtml\Grid;

use Lachestry\Faq\Api\FaqRepositoryInterface;
use Lachestry\Faq\Model\Faq;
use Lachestry\Faq\Model\FaqFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ObjectManager;

class Delete extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'Lachestry_Faq::delete';
    const MENU_ITEM = 'Lachestry_Faq::faq_admin_grid';
    const TITLE = 'Faq';

    private $faqFactory;
    private $faqRepository;
    private $resultRedirect;

    public function __construct(
        Action\Context $context,
        FaqFactory $faqFactory,
        FaqRepositoryInterface $faqRepository
    ) {
        $this->faqFactory = $faqFactory;
        $this->faqRepository = $faqRepository;
        parent::__construct($context);
        $this->resultRedirect = $this->resultRedirectFactory->create();
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam(Faq::ID);

        if (!$id) {
            $this->messageManager->addErrorMessage(__('We can\'t find a FAQ to delete.'));
            return $this->resultRedirect->setPath('*/*/');
        }

        try {
            $this->faqRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('The faq has been deleted.'));
            return $this->resultRedirect->setPath('*/*/');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->resultRedirect->setPath('*/*/edit', [Faq::ID => $id]);
        }
    }
}
